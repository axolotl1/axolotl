import os
import os.path
import sys
from setuptools import setup, find_packages
import subprocess

PACKAGE_NAME = 'axolotl'
MINIMUM_PYTHON_VERSION = 3, 6


def check_python_version():
    """Exit when the Python version is too low."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        sys.exit("Python {}.{}+ is required.".format(*MINIMUM_PYTHON_VERSION))


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    raise KeyError("'{0}' not found in '{1}'".format(key, module_path))


check_python_version()
version = read_package_variable('__version__')
description = read_package_variable('__description__')
setup(
    name=PACKAGE_NAME,
    version=version,
    description=version,

    packages=find_packages(exclude=['tests*']),
    license='Apache-2.0',
    classifiers=[
          'License :: OSI Approved :: Apache Software License',
    ],
    install_requires=[
        'pandas==1.0.3',
        'scikit-learn==0.22.2.post1',
        'grpcio',
        'grpcio-tools',
        'grpcio-testing',
        'ray<=1.0.1.post1',
        'networkx==2.4',
        'jupyterlab==2.2.9',
        'torch==1.4.0',
        'torchvision==0.5.0',
        'd3m==2020.11.3',
        'd3m-automl-rpc==1.0.0',
    ],
    extras_require={
        'cpu': ['tensorflow==2.2.0'],
        'gpu': ['tensorflow-gpu==2.2.0']
    }
)

try:
    subprocess.run(['pip', 'install', '-r', 'requirements.txt'])
except Exception as e:
    print(e)

