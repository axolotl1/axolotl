d3m==2020.11.3
-e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@627b3b533c3c9ec1fab53bdd54e77b7170712578#egg=sklearn_wrap
-e git+https://gitlab.com/datadrivendiscovery/common-primitives.git@c63c1788e5ca192be80ff368be79206b17612ff5#egg=common_primitives
-e git+https://gitlab.com/datadrivendiscovery/tests-data.git@753d974b322ccae79f674432a3ca79f2f734d4c2#egg=test_primitives&subdirectory=primitives
-e git+https://github.com/uncharted-distil/distil-primitives.git@ada286bd826efe7598b843fa53aca8abddda287c#egg=distil-primitives
-e git+https://github.com/usc-isi-i2/dsbox-primitives@1fe8beb0959f63971d4c7b7e1fc728192bb31a33#egg=dsbox-primitives
-e git+https://github.com/autonlab/autonbox.git@17a28bdfeb8cd175a366bd7eafbbdd07272b97b5#egg=autonbox
-e git+https://github.com/keras-team/keras-tuner.git@1.0.2rc0#egg=kerastuner
autokeras==1.0.3