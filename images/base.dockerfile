FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-stable-20201201-223410

RUN apt update

RUN pip3 install grpcio grpcio-tools grpcio-testing ray==1.0.1.post1 jupyterlab==2.2.9 d3m-automl-rpc==1.0.0
RUN pip3 install -e git+https://github.com/keras-team/keras-tuner.git@1.0.2rc0#egg=kerastuner
